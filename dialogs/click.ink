VAR num_clicks = 0

-> start

== start ==
{
  - num_clicks == 1: Oh, you clicked me!
  - num_clicks < 5: You clicked me again!
  - else: Please stop now… -> END
}
-> start
