using Godot;
using Ink;
using Ink.Runtime;

public class ClickableItem : ColorRect
{
    private Color _initialColor;
    private int _numClicks;
    private Story _story;

    public override void _Ready()
    {
        var storyFile = new File();
        storyFile.Open("res://dialogs/click.ink", File.ModeFlags.Read);
        try
        {
            var compiler = new Compiler(storyFile.GetAsText());
            _story = compiler.Compile();
        }
        finally
        {
            storyFile.Close();
        }
    }


    private void OnInput(object @event)
    {
        if (!(@event is InputEventMouseButton mouseEvent) || !mouseEvent.Pressed)
        {
            return;
        }

        if (!_story.canContinue)
        {
            return;
        }

        _numClicks++;
        _story.variablesState["num_clicks"] = _numClicks;
        var text = _story.Continue();

        var dialog = new AcceptDialog();
        dialog.WindowTitle = "";
        dialog.DialogText = text;
        AddChild(dialog);
        dialog.PopupCentered();
        
        if (!_story.canContinue)
        {
            Color = _initialColor;
            MouseDefaultCursorShape = CursorShape.Forbidden;
        }
    }

    private void OnMouseEntered()
    {
        if (!_story.canContinue)
        {
            return;
        }
        
        _initialColor = Color;
        Color = Colors.LightBlue;
    }


    private void OnMouseExited()
    {
        Color = _initialColor;
    }
}
